package funsets

object fun {
type Set = Int => Boolean
def contains(s: Set, elem: Int): Boolean = s(elem)//> contains: (s: funsets.fun.Set, elem: Int)Boolean
def singletonSet(elem:Int) :Set = x => x == elem  //> singletonSet: (elem: Int)funsets.fun.Set
val s1 = singletonSet(1)                          //> s1  : funsets.fun.Set = <function1>
contains(s1, 1)                                   //> res0: Boolean = true

//1,3,5,7,9
//5,7,9,11,13
//intersect: 5,7,9
//
//1,3
}