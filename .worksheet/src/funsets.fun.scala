package funsets

object fun {
type Set = Int => Boolean;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(106); 
def contains(s: Set, elem: Int): Boolean = s(elem);System.out.println("""contains: (s: funsets.fun.Set, elem: Int)Boolean""");$skip(49); 
def singletonSet(elem:Int) :Set = x => x == elem;System.out.println("""singletonSet: (elem: Int)funsets.fun.Set""");$skip(25); 
val s1 = singletonSet(1);System.out.println("""s1  : funsets.fun.Set = """ + $show(s1 ));$skip(16); val res$0 = 
contains(s1, 1);System.out.println("""res0: Boolean = """ + $show(res$0))}

//1,3,5,7,9
//5,7,9,11,13
//intersect: 5,7,9
//
//1,3
}
